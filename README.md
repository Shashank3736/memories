# Introduction
A social media to share your own created ai images.

# Features
- Complete Authentication
- Create, Read, Update, Delete Posts
- Like and save posts
- Infinite scroll in explore page
- Follow and unfollow users

# Technologies
- React
- TanStack Query
- TypeScript
- shadnui
- appwrite (for backend)

# Setup
1. Clone the repository
2. Install the dependencies
3. Change `.env.example` to `.env.local` and fill the required fields
4. Create an account on appwrite and create a project and fill the required fields in `.env.local`
5. Run the app

# Preview
> [Work in Progress]